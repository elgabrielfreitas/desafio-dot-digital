module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/demo/vue-store/' : '/',
   
    chainWebpack: config => {
        config
        .plugin('html')
        .tap(args => {
            args[0].title = 'Desafio DOT Digital Group'
            return args;
        });
    }
}