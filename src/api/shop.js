const _products = [
    {
        'id': 1, 
        'title': 'Nome do Filme', 
        'price': 79.99, 
        'inventory': 7, 
        'image': 'no-image.png',
        'categories': ['Movie'],
    },
    {
        'id': 2, 
        'title': 'Nome do Filme', 
        'price': 79.99, 
        'inventory': 7, 
        'image': 'no-image.png',
        'categories': ['Movie'], 
    },
    {
        'id': 3, 
        'title': 'Nome do Filme', 
        'price': 79.99, 
        'inventory': 7,     
        'image': 'no-image.png',
        'categories': ['Movie']
    },
    {
      'id': 3, 
      'title': 'Nome do Filme', 
      'price': 79.99, 
      'inventory': 7,       
      'image': 'no-image.png',
      'categories': ['Movie']
    },
    {
      'id': 3, 
      'title': 'Nome do Filme', 
      'price': 79.99, 
      'inventory': 7,       
      'image': 'no-image.png',
      'categories': ['Movie']
    },
    {
      'id': 3, 
      'title': 'Nome do Filme', 
      'price': 79.99, 
      'inventory': 7,       
      'image': 'no-image.png',
      'categories': ['Movie']
    }
]

export default {
  getProducts (cb, timeout = 100) {
    setTimeout(() => cb(_products), timeout)
  },

  buyProducts (products, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.webdriver)
        ? cb()
        : errorCb()
    }, 100)
  }
}